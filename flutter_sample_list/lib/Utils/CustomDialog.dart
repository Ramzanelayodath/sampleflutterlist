

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomDialog{
  showLoadingProgressDialog(BuildContext context) {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,dismissAfter: Duration(seconds: 5),textToBeDisplayed:'Loading...');
    return _progressDialog;
  }
}