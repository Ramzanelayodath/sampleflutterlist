import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sample_list/DataModel/ListModel.dart';
import 'package:http/http.dart' as http;
import 'Utils/CustomDialog.dart';
import 'Utils/Urls.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int list_count = 0;
  var contact_list = List<ListModel>();

  @override
  void initState() {
    // TODO: implement initState
    getContacts();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List"),
      ),
      body:ListView.builder(
        shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        itemCount: list_count,
        itemBuilder: (context, position) {
          return Padding(padding: EdgeInsets.only(left: 10,right: 10,top: 10),
            child:Container(
              width: double.infinity,
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30),
                          child: Image.network(contact_list[position].avatar),
                        ),
                        radius: 25,
                      ),
                      Padding(padding: EdgeInsets.only(left: 8),
                        child:Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(contact_list[position].name,style: TextStyle(fontSize: 15),),
                            Padding(padding: EdgeInsets.only(top: 3),
                            child: Text(contact_list[position].email,style: TextStyle(color: Colors.grey[800]),),)
                          ],
                        ),)
                    ],
                  ),
                  Divider()
                ],
              ),
            ) ,) ;
        },
      ) ,
    );
  }
   Future<void> getContacts() async {
     ProgressDialog dialog = CustomDialog().showLoadingProgressDialog(context);
     var response = await http.get(Uri.parse(Urls.LIST_URL));
     Map<String, dynamic> value = json.decode(response.body);
     if(response.statusCode == 200){
       dialog.dismissProgressDialog(context);
       try{
            var array = value['data'];
            for(int i = 0 ; i < array.length ; i++){
               var obj = array[i];
               contact_list.add(ListModel(obj['first_name'], obj['email'], obj['avatar']));
            }
            setState(() {
              list_count = contact_list.length;
            });
       }catch(e){}
     }else{
       dialog.dismissProgressDialog(context);
     }
   }
}
